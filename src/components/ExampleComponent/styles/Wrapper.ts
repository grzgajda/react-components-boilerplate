import styled from '../../../theme'

export const Wrapper = styled.div`
  width: 300px;
  height: 200px;

  display: flex;
  align-items: center;
  justify-content: center;
`
