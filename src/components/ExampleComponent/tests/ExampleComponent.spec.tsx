import '@testing-library/jest-dom/extend-expect'
import * as React from 'react'
import { render } from '@testing-library/react'
import ExampleComponent from '../ExampleComponent'

describe('<ExampleComponent />', () => {
  it('should render passed text', () => {
    const { getByText } = render(<ExampleComponent>Lorem ipsum dolor sit amet</ExampleComponent>)

    expect(getByText(/dolor sit amet/)).toBeInTheDocument()
  })
})
