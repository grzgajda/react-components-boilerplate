import { addDecorator, configure } from '@storybook/react'
import { addReadme } from 'storybook-readme'
import { withKnobs } from '@storybook/addon-knobs'

addDecorator(addReadme)
addDecorator(withKnobs)

const req = require.context('../src', true, /\.stories\.tsx?$/)
function loadStories() {
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)
