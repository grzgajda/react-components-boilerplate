# Changelog

## 0.0.5 (15.12.2019)
- upgraded dependencies

## 0.0.4 (15.12.2019)
- ignore test, spec and stories files from Babel output

## 0.0.3 (12.12.2019)
- updated README.md

## 0.0.2 (13.11.2019)
- configured GitLab CI with artifacts: transpiled components and storybook
- created `ExampleComponent`
- created initial configuration for `styled-components`

## 0.0.1 (13.11.2019)
- configured Storybook to support TypeScript
- configured ESLint to support React & TypeScript
- configured Prettier to support ESLint
- configured Babel 7 with minifier