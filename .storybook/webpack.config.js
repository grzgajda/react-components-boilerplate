const path = require('path')
const appSourceDir = path.join(__dirname, '..', 'src')

module.exports = ({ config }) => {
  /**
   * Replace original storybook svg loader with custom one
   * to load SVG files into components.
   */
  const svgRule = config.module.rules.find((rule) => 'test.svg'.match(rule.test));
  svgRule.exclude = [ appSourceDir ];

  config.module.rules.push({
    test: /\.svg$/i,
    include: [ appSourceDir ],
    use: [ '@svgr/webpack', 'url-loader' ]
  });

  /**
   * Configure storybook to understand Typescript files
   * using babel configuration.
   */
  config.module.rules.push({
    test: /\.(ts|tsx)$/,
    use: [
      {
        loader: require.resolve('babel-loader'),
        options: {
          presets: [require.resolve('babel-preset-react-app')],
        },
      }
    ],
  })

  config.resolve.extensions.push('.ts', '.tsx')
  return config
}
