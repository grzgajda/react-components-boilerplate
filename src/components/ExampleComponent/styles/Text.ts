import styled from '../../../theme'

export const Text = styled.p`
  font-size: 36px;
  text-transform: uppercase;
`
