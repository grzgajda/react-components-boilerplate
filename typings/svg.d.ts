declare module "*.svg" {
  const url: string;
  const component: React.FunctionComponent

  export default url
  export { component as ReactComponent }
}