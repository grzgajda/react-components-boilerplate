import * as React from 'react'
import { Wrapper } from './styles/Wrapper'
import { Text } from './styles/Text'

export interface ExampleComponentProps {
  children: React.ReactNode
}

const ExampleComponent: React.FunctionComponent = ({ children }: ExampleComponentProps) => (
  <Wrapper>
    <Text>{children}</Text>
  </Wrapper>
)

export default ExampleComponent
