# ExampleComponent

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vitae maximus mi, vel ultricies dui. Praesent nec efficitur risus. Vivamus sed dolor pretium, mattis sapien id, facilisis tortor. Nulla pulvinar viverra quam, vel lacinia ex viverra quis. Integer eleifend lobortis ex, sed ullamcorper libero vestibulum non. Cras et augue gravida, tempus magna nec, egestas urna. Fusce ex lorem, vehicula nec nibh et, sollicitudin luctus ligula. Nullam ut enim velit. Sed nec felis mattis, faucibus est nec, sagittis metus. Nulla facilisi.

```typescript
import * as React from 'react'
import { ExampleComponent } from './src/ExampleComponent'

function Render() {
  return (
    <ExampleComponent>Lorem ipsum dolor sit amet</ExampleComponent>
  )
}
```

Curabitur dictum, ex eu tincidunt sollicitudin, erat libero bibendum lacus, quis efficitur lorem nisl ac quam. In dapibus metus quis tortor condimentum convallis. Ut magna magna, iaculis ut nisi vel, dictum venenatis nunc. Curabitur sit amet ultricies eros, non aliquet mauris. Proin mi ligula, laoreet sit amet sem at, pharetra sagittis risus. Sed at nulla commodo, bibendum mi a, iaculis felis. Proin scelerisque leo eget odio scelerisque interdum. Ut vehicula quis magna sed mollis. Aenean sodales sit amet odio vitae porttitor. Nunc nec velit nec ante sodales vestibulum.