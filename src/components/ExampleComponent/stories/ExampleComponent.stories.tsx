import * as React from 'react'
import { storiesOf } from '@storybook/react'
import { text } from '@storybook/addon-knobs'
import ExampleComponent from '../ExampleComponent'
import readmeFile from '../README.md'

storiesOf('UI | ExampleComponent', module)
  .addParameters({
    readme: {
      sidebar: readmeFile,
    },
  })
  .add('default', () => (
    <ExampleComponent>{text('children', 'Lorem ipsum dolor sit amet')}</ExampleComponent>
  ))
