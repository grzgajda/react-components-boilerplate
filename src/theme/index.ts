import * as styledComponents from 'styled-components'

const {
  default: styled,
  css,
  keyframes,
  ThemeProvider,
} = styledComponents as styledComponents.ThemedStyledComponentsModule<{}>

export default styled
export { css, keyframes, ThemeProvider }
